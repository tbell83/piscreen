const url="https://nodered.home.tombell.io";

function log(msg) {
  console.log(`${epoch()}: ${msg}`)
}

function epoch() {
  const date = new Date();
  return Math.floor( date.getTime() / 1000);
}

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", `${theUrl}?_=${epoch()}`, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

function updateTime() {
  var refresh = 1000;
  update = setTimeout('showTime()', refresh)
}

function updateDate() {
  var refresh = 60000;
  update = setTimeout('showDate()', refresh)
}

function updateWeather() {
  var refresh = 60000;
  update = setTimeout('showWeather()', refresh)
}

function showTime() {
  var dt = new Date();
  let hour = `${dt.getHours() < 10 ? 0 : ''}${dt.getHours()}`;
  let minute = `${dt.getMinutes() < 10 ? 0 : ''}${dt.getMinutes()}`;
  let second = `${dt.getSeconds() < 10 ? 0 : ''}${dt.getSeconds()}`;
  let payload = `${hour}:${minute}:${second}`
  // log(payload);
  document.getElementById('time').innerHTML = payload;
  updateTime();
}

function showDate() {
  var dt = new Date();
  let day = `${dt.getDate() < 10 ? 0 : ''}${dt.getDate()}`;
  let month = `${dt.getMonth() < 9 ? 0 : ''}${dt.getMonth() + 1}`;
  let year = dt.getFullYear();
  let payload = `${year}/${month}/${day}`;
  log(payload);
  document.getElementById('date').innerHTML = payload;
  updateDate();
}

function showWeather () {
  var weather = JSON.parse(httpGet(`${url}/weather`));
  console.log(weather)
  let icon = getWeatherIcon(weather.condition)
  let payload = `${icon}${weather.temperature} ${weather.temperature_unit}`
  log(payload);
  document.getElementById('currentWeather').innerHTML = payload;
  updateWeather();
}

function getWeatherIcon(weatherCondition) {
  var daytime = (httpGet(`${url}/day_night`).trim() === "day");

  switch (weatherCondition) {
    case 'sunny':
      iconFile = 'day';
      break;
    case 'cloudy':
      iconFile = 'cloudy';
      break;
    case 'partlycloudy':
      iconFile = 'cloudy-day-1';
      break;
    case 'rainy':
      iconFile = 'rainy-1';
      break;
  }

  if (!daytime) {
    iconFile = iconFile.replace('day', 'night');
  }

  return `<img class="weatherImage" src=img/static/${iconFile}.svg>`
}

function pageLoad() {
  showDate();
  showTime();
  showWeather();
}